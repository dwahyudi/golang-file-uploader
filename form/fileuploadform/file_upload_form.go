package fileuploadform

import (
	"mime/multipart"
)

func FileUpload(file multipart.File, fileHandler *multipart.FileHeader, u FileSaveable, v Validatable, m MetadataStorable, userAgent string) []error {
	errors := v.validate(file, fileHandler)

	if len(errors) == 0 {
		u.save(file, fileHandler)
		m.store(fileHandler, userAgent)
	}

	return errors
}
