package fileuploadform

import (
	"errors"
	"mime/multipart"

	"github.com/thoas/go-funk"
)

type Validatable interface {
	validate(file multipart.File, fileHandler *multipart.FileHeader) []error
}

type ImageValidation struct {
}

func (i ImageValidation) validate(file multipart.File, fileHandler *multipart.FileHeader) []error {
	fileSize := fileHandler.Size
	errs := []error{}
	if fileSize > 8_000_000 {
		errs = append(errs, errors.New("Image max size is 8 Megabytes"))
	}

	contentType := fileHandler.Header["Content-Type"]
	allowedImageTypes := []string{"image/png", "image/jpeg"}
	if !funk.Contains(allowedImageTypes, contentType[0]) {
		errs = append(errs, errors.New("File type required: images (jpeg and png)"))
	}

	return errs
}
