package fileuploadform

import (
	"mime/multipart"
	"time"

	"github.com/dwahyudi/golang-file-uploader/util"
)

type MetadataStorable interface {
	store(fileHandler *multipart.FileHeader, u string) (int64, error)
}

type MysqlMetadata struct {
}

func (m MysqlMetadata) store(fileHandler *multipart.FileHeader, userAgent string) (int64, error) {
	db := util.MysqlConn()
	defer db.Close()

	query := "INSERT INTO metadata (filename, content_type, size, created_at, user_agent) VALUES(?, ?, ?, ?, ?);"
	stmt, stmtErr := db.Prepare(query)
	util.LogPanic(stmtErr)

	contentType := fileHandler.Header["Content-Type"][0]
	res, queryErr := stmt.Exec(fileHandler.Filename, contentType, fileHandler.Size, time.Now().UTC(), userAgent)
	util.LogPanic(queryErr)

	id, getLastInsertIdErr := res.LastInsertId()
	util.LogPanic(getLastInsertIdErr)

	return id, queryErr
}
