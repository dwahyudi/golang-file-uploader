package fileuploadform

import (
	"io"
	"mime/multipart"
	"os"

	"github.com/dwahyudi/golang-file-uploader/util"
)

type FileSaveable interface {
	save(file multipart.File, fileHandler *multipart.FileHeader)
}

type FolderFileSave struct {
}

func (f FolderFileSave) save(file multipart.File, fileHandler *multipart.FileHeader) {
	defer file.Close()

	filenameWithHash := util.RandomString(6) + "_" + fileHandler.Filename
	dest, err := os.Create("temp/images/" + filenameWithHash)
	defer dest.Close()
	util.LogPanic(err)

	_, err = io.Copy(dest, file)
	util.LogPanic(err)
}
