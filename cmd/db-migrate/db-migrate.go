package main

import (
	"database/sql"
	"fmt"
	"os"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Please specify step. ex: go run main.go 2")
		return
	}

	steps, err := strconv.Atoi(os.Args[1])

	if err != nil {
		fmt.Println("Step have to be a number. ex: go run main.go 2")
		return
	}

	m := prepareDB()

	m.Steps(steps)
	fmt.Println("db migrate completed...")
}

func prepareDB() *migrate.Migrate {
	db, _ := sql.Open("mysql", os.Getenv("GO_FILE_UPLOADER")+"?multiStatements=true")
	driver, _ := mysql.WithInstance(db, &mysql.Config{})
	m, _ := migrate.NewWithDatabaseInstance(
		"file://migrations",
		"mysql",
		driver,
	)

	return m
}
