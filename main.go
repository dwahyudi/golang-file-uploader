package main

import (
	"log"

	"github.com/dwahyudi/golang-file-uploader/web"
)

func main() {
	log.Print("== Starting app ==")
	web.LaunchWeb()
}
