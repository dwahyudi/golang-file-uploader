package handler

import (
	"net/http"
	"os"
	"path"
	"text/template"

	"github.com/dwahyudi/golang-file-uploader/util"

	"github.com/dwahyudi/golang-file-uploader/form/fileuploadform"
)

const MAX_UPLOAD_SIZE = 1024 * 1024 // 1MB

func FileUploadNewHandler(w http.ResponseWriter, req *http.Request) {
	flash, err := util.GetFlash(w, req, "flash")
	util.LogPanic(err)

	flashPresent := (flash != nil)

	var filepath = path.Join("web", "templates", "file_upload", "new.html")
	tmpl, err := template.ParseFiles(filepath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = tmpl.Execute(w, map[string]interface{}{
		"title":        "Image Upload",
		"flash":        string(flash),
		"FlashPresent": flashPresent,
		"auth":         os.Getenv("GO_FILE_UPLOADER_AUTH"),
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func FileUploadCreateHandler(w http.ResponseWriter, req *http.Request) {
	file, fileHandler, err := req.FormFile("uploaded-file")
	userAgent := req.Header["User-Agent"][0]

	if err != nil {
		http.Error(w, "Uploaded file is missing", http.StatusInternalServerError)
		return
	}

	if req.FormValue("auth") != os.Getenv("GO_FILE_UPLOADER_AUTH") {
		http.Error(w, "Wrong auth key", http.StatusInternalServerError)
		return
	}

	req.Body = http.MaxBytesReader(w, req.Body, MAX_UPLOAD_SIZE)
	if err := req.ParseMultipartForm(MAX_UPLOAD_SIZE); err != nil {
		http.Error(w, "File size too big", http.StatusBadRequest)
		return
	}

	errs := fileuploadform.FileUpload(
		file,
		fileHandler,
		&fileuploadform.FolderFileSave{},
		&fileuploadform.ImageValidation{},
		&fileuploadform.MysqlMetadata{},
		userAgent,
	)

	var message string
	if len(errs) > 0 {
		for _, eachErr := range errs {
			message += eachErr.Error() + ", "
		}
	} else {
		message = "file uploaded successfully"
	}

	util.SetFlash(w, "flash", []byte(message))
	http.Redirect(w, req, "/file_uploads/new", http.StatusMovedPermanently)
}
