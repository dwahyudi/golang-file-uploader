package web

import (
	"fmt"
	"log"
	"net/http"

	"github.com/dwahyudi/golang-file-uploader/handler"
)

func LaunchWeb() {
	http.HandleFunc("/ping", pingHandler)
	http.HandleFunc("/file_uploads/new", handler.FileUploadNewHandler)
	http.HandleFunc("/file_uploads/", handler.FileUploadCreateHandler)

	port := ":8080"
	log.Print("== Starting web at " + port + " ==")
	if err := http.ListenAndServe(port, nil); err != nil {
		log.Fatal(err)
	}
}

func pingHandler(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "pong")
}
