package util

import (
	"log"
	"math/rand"
	"time"
)

func LogPanic(err error) {
	if err != nil {
		log.Panic(err)
	}
}

func RandomString(length int) string {
	const charset = "abcdefghijklmnopqrstuvwxyz1234567890"

	seed := rand.New(rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seed.Intn(len(charset))]
	}
	return string(b)
}
