package util

import (
	"database/sql"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

func MysqlConn() *sql.DB {
	db, err := sql.Open("mysql", os.Getenv("GO_FILE_UPLOADER")+"?multiStatements=true")
	LogPanic(err)

	return db
}
